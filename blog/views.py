from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
from .forms import PostForm
from .models import PostModel

def delete(request, delete_id):
    PostModel.objects.filter(id=delete_id).delete()
    return HttpResponseRedirect("/blog/list")

def update(request, update_id):
    akun_update = PostModel.objects.get(id=update_id)
    data = {
        'Mata_Kuliah' : akun_update.Mata_Kuliah,
        'Deskripsi': akun_update.Deskripsi,
        'Dosen' : akun_update.Dosen,
        'Ruang_Kelas' : akun_update.Ruang_Kelas,
        'Jumlah_SKS' : akun_update.Jumlah_SKS,
        'semester' : akun_update.semester,
    }
    post_form = PostForm(request.POST or None, initial=data, instance=akun_update)

    if request.method == 'POST':
        #cara ambil data dari input user ke database dg validasi
        if post_form.is_valid():
            post_form.save()
        return HttpResponseRedirect("/blog/list")

    
    context = {
        'pageTitle' : 'Update Form',
        'post_form' : post_form,
    }
    return render(request, 'blog/create.html', context)

def index(request):
    posts = PostModel.objects.all()
    context = {
        'heading' : 'Post',
        'contents': 'List Post',
        'posts' : posts,
    }
    return render(request, 'blog/list.html', context)

def create(request):
    post_form = PostForm(request.POST or None)
    if request.method == 'POST':
        #cara ambil data dari input user ke database dg validasi
        if post_form.is_valid():
            post_form.save()
        return HttpResponseRedirect("/blog/list")

    
    context = {
        'pageTitle' : 'Create Form',
        'post_form' : post_form,
    }
    return render(request, 'blog/create.html', context)

def detail(request, detail_id):
    akun = PostModel.objects.get(id=detail_id)
    akun_matkul = PostModel.objects.filter(id=detail_id)
    data = {
        'matkul' : akun_matkul
    }
    return render(request, 'blog/detail.html', data)

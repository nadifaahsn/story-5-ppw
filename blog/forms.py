from django import forms

#import model dari models.py
from .models import PostModel

class PostForm(forms.ModelForm):
    class Meta:
        model = PostModel
        fields = [
            'Mata_Kuliah',
            'Deskripsi',
            'Dosen',
            'Ruang_Kelas',
            'Jumlah_SKS',
            'semester',
        ]
        widgets = {
            'Mata_Kuliah': forms.TextInput(  
                attrs={
                    'class' : 'form-control', #buat isi class sesuai bootstrapnya
                    'placeholder' : 'Mata kuliah',
                }
            ),

            'Deskripsi': forms.TextInput(  
                attrs={
                    'class' : 'form-control', #buat isi class sesuai bootstrapnya
                    'placeholder' : 'Deskripsi mengenai mata kuliah',
                }
            ),

            'Dosen': forms.TextInput(  
                attrs={
                    'class' : 'form-control', #buat isi class sesuai bootstrapnya
                    'placeholder' : 'nama dosen',
                }
            ),

            'Ruang_Kelas': forms.TextInput(  
                attrs={
                    'class' : 'form-control', #buat isi class sesuai bootstrapnya
                }
            ),

            'Jumlah_SKS': forms.TextInput(  
                attrs={
                    'class' : 'form-control', #buat isi class sesuai bootstrapnya
                }
            ),

            'semester': forms.Select(  
                attrs={
                    'class' : 'form-control', #buat isi class sesuai bootstrapnya
                }
            ),
        }

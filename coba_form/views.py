from django.shortcuts import render

# Create your views here.
# from .forms import FormField

def index(request):
    return render(request, 'index.html')